# README #

This application represents the implementation of workflow web application for Behavox.
### Source Code ###

* [Link to BitBucket repo](https://jawadali@bitbucket.org/jawadali/behavox-api-issue-tracking.git)

### Project Setup ###
It's a spring-boot application using spring-data-jpa with Cassandra DB. It requires Cassandra to be installed and running locally.
Please Create a local keyspace named 'behavox' before running the application.
##### NOTE #####
We know that ideally Cassandra is not the best choice with a project of this scale but since this is just a demo application, the choice of database should not be a factor to decide on the design/implementation decisions of this demo. 

### Implementation Details ###

The focus of the project has been solely the implementation of the workflow part of the task. I acknowledge that a lot has been left out but that's because of the time required to complete everthing to make it a fully functional app. Here are some quick details about the implementation, scope and limitations of this project.

 __DefaultStatusCreationController.java__ is a quick alternative for a proper migration script to create some default workflow as described in the task description. In an ideal scenario, this should never be done like this. The only REST endpoint in this controller should be run the first thing so that we can have some default workflow before creating issues.

We can use the REST endpoints defined in __StatusController.java__ to do basic __CRUD__ operations on statuses. 

__TransitionController.java__ has the REST endpoints to add or remove next possible statuses from a given status. This is our way of creating customized workflow.

__IssueController.java__ implements the REST endpoints for issue __CRUD__ as well as assignment and unassignment of issues.

### The 'asyc operations' part ###
For this, I added an empty __NotificationService.java__. This is just to show that something could be done. We could either use some queuing mechanism and submit the notification to queue or in a microservices architecture, we could use an HTTP client to send the call to our __Notification Microservice__

In another application, we could also send Events via queues so that other microservices listening to these events can perform whatever processing they need to do for that event.

### What did I acknowledge but could not complete ###

The whole CRUD around users, permissions, controller tests (VERY IMPORTANT), proper test coverage (something like jacoco), proper versioning of the DB migrations, proper migration script for initial data (for default workflow).