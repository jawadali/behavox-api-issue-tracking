package com.behavox.workflow.dao;

import java.util.UUID;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import com.behavox.workflow.model.Issue;

@Repository
public interface IssueRepository extends CassandraRepository<Issue, UUID> {
}
