package com.behavox.workflow.dao;

import java.util.UUID;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import com.behavox.workflow.model.Status;

@Repository
public interface StatusRepository extends CassandraRepository<Status, UUID> {

}
