package com.behavox.workflow.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;

import com.datastax.driver.core.AuthProvider;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.PlainTextAuthProvider;
import com.datastax.driver.core.QueryOptions;
import com.datastax.driver.core.Session;
import com.datastax.driver.mapping.MappingManager;

@Configuration
@EnableCassandraRepositories(
        basePackages = "com.behavox.workflow.dao")
public class CassandraConfig {

    @Value("${spring.data.cassandra.keyspace-name}")
    private String keyspace;

    @Value("${spring.data.cassandra.contact-points}")
    private String contactpoints;

    @Value("${spring.data.cassandra.port:9042}")
    private Integer port;

    @Value("${spring.data.cassandra.username}")
    private String username;

    @Value("${spring.data.cassandra.password}")
    private String password;

    @Value("${spring.data.cassandra.ssl:false}")
    private Boolean sslEnabled;

    @Value("${spring.data.cassandra.consistency-level}")
    private String consistencyLevel;

    @Bean
    public Cluster cluster() {

        String[] contactpoints = this.contactpoints.split(",");

        AuthProvider authProvider = new PlainTextAuthProvider(username, password);
        Cluster.Builder builder = Cluster.builder()
                .addContactPoints(contactpoints)
                .withoutJMXReporting()
                .withPort(port)
                .withAuthProvider(authProvider)
                .withQueryOptions(new QueryOptions()
                        .setConsistencyLevel(ConsistencyLevel.valueOf(consistencyLevel.toUpperCase())));

        if(sslEnabled){
            builder.withSSL();
        }
        return builder.build();
    }

    @Bean
    public Session session(Cluster cluster) {
        return cluster.connect(keyspace);
    }

    @Bean
    MappingManager mappingManager(Session session){
        return new MappingManager(session);
    }
}