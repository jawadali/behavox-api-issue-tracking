package com.behavox.workflow.config;

import com.behavox.workflow.model.Issue;
import com.behavox.workflow.model.Status;
import com.datastax.driver.core.Session;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Ideally, this should be versioned so that so that you can pass a version number in each
 * migration. This will allow the migrations to run EXACTLY ONCE and never again.
 */

@Component
@Slf4j
public class MigrationExecutor {

    @Autowired
    private Session session;

    @PostConstruct
    public void postConstruct() {
        createStatusTable();
        createIssueTable();
    }

    public void createStatusTable() {
        StringBuilder sb = new StringBuilder("CREATE TABLE IF NOT EXISTS ")
            .append(Status.TABLE_NAME).append(" (")
            .append("id uuid, ")
            .append("name text, ")
            .append("transitions set<uuid>, ")
            .append("PRIMARY KEY ((id), name))");

        String query = sb.toString();
        log.info("********** Starting Cassandra migration for status TABLE ********");
        log.info(query);
        session.execute(query);
        log.info("********** Finished Cassandra migration for status TABLE ********");

    }

    public void createIssueTable() {
        StringBuilder sb = new StringBuilder("CREATE TABLE IF NOT EXISTS ")
            .append(Issue.TABLE_NAME).append(" (")
            .append("id uuid, ")
            .append("statusId uuid, ")
            .append("title text, ")
            .append("description text, ")
            .append("assigneeId text, ")
            .append("reporterId text, ")
            .append("PRIMARY KEY (id))");

        String query = sb.toString();
        log.info("********** Starting Cassandra migration for issue TABLE ********");
        log.info(query);
        session.execute(query);
        log.info("********** Finished Cassandra migration for issue TABLE ********");

    }
}
