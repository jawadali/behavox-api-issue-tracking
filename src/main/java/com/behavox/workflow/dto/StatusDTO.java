package com.behavox.workflow.dto;

import java.util.Set;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class StatusDTO {

    private String id;

    @NotEmpty
    private String name;

    @JsonProperty(access = Access.READ_ONLY)
    private Set<String> transitions;
}
