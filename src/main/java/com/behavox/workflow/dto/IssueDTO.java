package com.behavox.workflow.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class IssueDTO {

    private String id;
    @NotEmpty
    private String title;
    @NotEmpty
    private String description;
    private UserDTO assignee;
    private UserDTO reporter;
    /**
     * All new issues are created with OPEN status so this should be READ_ONLY
     * to avoid creating issues with a predefined status
     */
    @JsonProperty(access = Access.READ_ONLY)
    private String statusId;

}
