package com.behavox.workflow.enricher;

import com.behavox.workflow.dto.IssueDTO;
import com.behavox.workflow.dto.UserDTO;
import com.behavox.workflow.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class IssueEnricher {

    @Autowired
    private UserService userService;

    /**
     * Enrich the issueDTO with basic user details of first name, last name and avatarUrl.
     * Ideally, there could be other fields that would need enrichment as well.
     */

    public void enrich(IssueDTO issueDTO) {

        UserDTO assignee = userService.getUserById(issueDTO.getAssignee().getId());
        issueDTO.getAssignee().setFirstName(assignee.getFirstName());
        issueDTO.getAssignee().setLastName(assignee.getLastName());
        issueDTO.getAssignee().setAvatarUrl(assignee.getAvatarUrl());

        UserDTO reporter = userService.getUserById(issueDTO.getReporter().getId());
        issueDTO.getReporter().setFirstName(reporter.getFirstName());
        issueDTO.getReporter().setLastName(reporter.getLastName());
        issueDTO.getReporter().setAvatarUrl(reporter.getAvatarUrl());
    }
}
