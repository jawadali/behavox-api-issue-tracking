package com.behavox.workflow.controllers;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.behavox.workflow.dto.StatusDTO;
import com.behavox.workflow.service.StatusService;

@RestController
@RequestMapping(value = "/api/v1/statuses")
public class StatusController {

    @Autowired
    private StatusService statusService;

    @PostMapping
    public ResponseEntity<StatusDTO> createStatus(@RequestBody @Valid StatusDTO statusDTO) {
        return new ResponseEntity<>(statusService.createStatus(statusDTO), HttpStatus.CREATED);
    }

    /**
     * This will be useful for frontEnd in some cases so I added it here.
     * In real world scenario, we won't have that many statuses so it's ok to return everything in one call without pagination.
     * @return
     */
    @GetMapping
    public ResponseEntity<List<StatusDTO>> getStatuses() {
        return new ResponseEntity<>(statusService.getAllStatuses(), HttpStatus.OK);
    }

    @GetMapping(value = "/{statusId}")
    public ResponseEntity<StatusDTO> getStatus(@PathVariable @NotEmpty @Value("statusId") String statusId) {
        return new ResponseEntity<>(statusService.getStatus(statusId), HttpStatus.OK);
    }

    @PutMapping(value = "/{statusId}")
    public ResponseEntity<StatusDTO> updateStatus(@PathVariable @NotEmpty @Value("statusId") String statusId, @RequestBody @Valid StatusDTO statusDTO) {
        return new ResponseEntity<>(statusService.updateStatus(statusId, statusDTO), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{statusId}")
    public ResponseEntity<Void> deleteStatus(@PathVariable @NotEmpty @Value("statusId") String statusId) {
        statusService.deleteStatus(statusId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
