package com.behavox.workflow.controllers;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

import com.behavox.workflow.dto.AssignmentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.behavox.workflow.dto.IssueDTO;
import com.behavox.workflow.dto.TransitionDTO;
import com.behavox.workflow.service.IssueService;

@RestController
@RequestMapping(value = "/api/v1/issues")
public class IssueController {

    @Autowired
    private IssueService issueService;

    @PostMapping
    public ResponseEntity<IssueDTO> createIssue(@RequestBody @Valid IssueDTO issueDTO) {
        return new ResponseEntity<>(issueService.createIssue(issueDTO), HttpStatus.CREATED);
    }

    @GetMapping(value = "/{issueId}")
    public ResponseEntity<IssueDTO> getIssue(@PathVariable @NotEmpty @Value("issueId") String issueId) {
        return new ResponseEntity<>(issueService.getIssue(issueId), HttpStatus.OK);
    }

    @PutMapping(value = "/{issueId}")
    public ResponseEntity<IssueDTO> updateIssue(@PathVariable @NotEmpty @Value("issueId") String issueId, @RequestBody @Valid IssueDTO issueDTO) {
        return new ResponseEntity<>(issueService.updateIssue(issueId, issueDTO), HttpStatus.OK);
    }

    @DeleteMapping(value = "{issueId}")
    public ResponseEntity<Void> deleteIssue(@PathVariable @NotEmpty @Value("issueId") String issueId) {
        issueService.deleteIssue(issueId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping(value = "/{issueId}/assign")
    public ResponseEntity<IssueDTO> assignIssue(@PathVariable @NotEmpty @Value("issueId") String issueId, @RequestBody @Valid AssignmentDTO assignmentDTO) {
        return new ResponseEntity<>(issueService.assignIssue(issueId, assignmentDTO), HttpStatus.OK);
    }

    @PutMapping(value = "/{issueId}/unassign")
    public ResponseEntity<IssueDTO> assignIssue(@PathVariable @NotEmpty @Value("issueId") String issueId) {
        return new ResponseEntity<>(issueService.unassignIssue(issueId), HttpStatus.OK);
    }

    @PutMapping("/{issueId}/status")
    public ResponseEntity<IssueDTO> changeIssueStatus(@PathVariable @NotEmpty @Value("issueId") String issueId, @RequestBody @Valid TransitionDTO transitionDTO) {
        return new ResponseEntity<>(issueService.changeIssueStatus(issueId, transitionDTO), HttpStatus.OK);
    }
}
