package com.behavox.workflow.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.behavox.workflow.dto.TransitionDTO;
import com.behavox.workflow.service.TransitionService;

@RestController
@RequestMapping(value = "/api/v1/statuses/{statusId}/transitions")
public class TransitionController {

    @Autowired
    private TransitionService transitionService;

    @PutMapping(value = "/add")
    public ResponseEntity<Void> addTransition(@PathVariable(name = "statusId") String statusId, @Valid @RequestBody TransitionDTO transitionDTO) {
        transitionService.addTransition(statusId, transitionDTO);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(value = "/remove")
    public ResponseEntity<Void> removeTransition(@PathVariable(name = "statusId") String statusId, @Valid @RequestBody TransitionDTO transitionDTO) {
        transitionService.removeTransition(statusId, transitionDTO);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
