package com.behavox.workflow.controllers;

import com.behavox.workflow.dto.StatusDTO;
import com.behavox.workflow.service.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/v1/statuses/default")
public class DefaultStatusCreationController {

    @Autowired
    private StatusService statusService;

    /**
     * This is not the right way to insert default data into the database. Ideally,
     * this should've been a versioned migration script which should run EXACTLY ONCE,
     * but due to lack of time I'm using this lazy approach to create the default workflow.
     */
    @PostMapping
    public ResponseEntity<List<StatusDTO>> createDefaultStatuses() {
        return new ResponseEntity(statusService.createDefaultStatuses(), HttpStatus.CREATED);
    }
}
