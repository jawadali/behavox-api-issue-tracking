package com.behavox.workflow.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.behavox.workflow.dto.IssueDTO;
import com.behavox.workflow.model.Issue;
import com.behavox.workflow.util.UUIDUtil;

@Component
public class IssueDTO2IssueConverter implements Converter<IssueDTO, Issue> {
    @Override
    public Issue convert(IssueDTO issueDTO) {
        return Issue.builder()
                .id(StringUtils.isEmpty(issueDTO.getId()) ? null : UUIDUtil.getUUIDFromString(issueDTO.getId()))
                .title(issueDTO.getTitle())
                .description(issueDTO.getDescription())
                //it's ok if assigneeId is null here, we will assign system default userId to it in the service.
                .assigneeId(issueDTO.getAssignee() == null ? null : issueDTO.getAssignee().getId())
                //ideally, frontend should send the reporterId since it's the logged in user. But just in case it doesn't we will set it in the service from AuthenticationService
                .reporterId(issueDTO.getReporter() == null ? null : issueDTO.getReporter().getId())
                .statusId(StringUtils.isEmpty(issueDTO.getId()) ? null : UUIDUtil.getUUIDFromString(issueDTO.getStatusId()))
                .build();
    }
}
