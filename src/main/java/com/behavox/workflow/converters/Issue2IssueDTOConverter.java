package com.behavox.workflow.converters;

import com.behavox.workflow.dto.UserDTO;
import org.apache.catalina.User;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.behavox.workflow.dto.IssueDTO;
import com.behavox.workflow.model.Issue;
import org.springframework.util.StringUtils;

@Component
public class Issue2IssueDTOConverter implements Converter<Issue, IssueDTO> {
    @Override
    public IssueDTO convert(Issue issue) {
        return IssueDTO.builder()
                .id(issue.getId().toString())
                .title(issue.getTitle())
                .description(issue.getDescription())
                //assigneeId will never be null because we always assign default system user if there is no assignee
                //just fill the id here, we will enrich the other fields via UserEnricher.
                .assignee(UserDTO.builder().id(issue.getAssigneeId()).build())
                //reporter will never be null because it will always be the user who created it.
                .reporter(UserDTO.builder().id(issue.getReporterId()).build())
                .statusId(issue.getStatusId().toString())
                .build();
    }
}
