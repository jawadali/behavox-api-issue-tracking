package com.behavox.workflow.converters;

import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.behavox.workflow.dto.StatusDTO;
import com.behavox.workflow.model.Status;
import com.behavox.workflow.util.UUIDUtil;
import com.google.common.collect.Sets;

@Component
public class StatusDTO2StatusConverter implements Converter<StatusDTO, Status> {

    @Override
    public Status convert(StatusDTO statusDTO) {
        return Status.builder()
                .id(StringUtils.isEmpty(statusDTO.getId()) ? null : UUIDUtil.getUUIDFromString(statusDTO.getId()))
                .name(statusDTO.getName())
                .transitions(CollectionUtils.isEmpty(statusDTO.getTransitions()) ? Sets.newHashSet() : statusDTO.getTransitions().stream().map(s -> UUID.fromString(s)).collect(Collectors.toSet()))
                .build();
    }
}
