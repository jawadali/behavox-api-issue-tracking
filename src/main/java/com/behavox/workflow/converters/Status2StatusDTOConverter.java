package com.behavox.workflow.converters;

import java.util.stream.Collectors;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.behavox.workflow.dto.StatusDTO;
import com.behavox.workflow.model.Status;
import com.google.common.collect.Sets;

@Component
public class Status2StatusDTOConverter implements Converter<Status, StatusDTO> {
    @Override
    public StatusDTO convert(Status status) {
        return StatusDTO.builder()
                .id(status.getId().toString())
                .name(status.getName())
                .transitions(CollectionUtils.isEmpty(status.getTransitions()) ? Sets.newHashSet() : status.getTransitions().stream().map(t -> t.toString()).collect(Collectors.toSet()))
                .build();
    }
}
