package com.behavox.workflow;

public enum NotificationTemplate {

    ISSUE_CREATED,
    ISSUE_UPDATED,
    ISSUE_ASSIGNED,
    ISSUE_CHANGE_STATUS
}
