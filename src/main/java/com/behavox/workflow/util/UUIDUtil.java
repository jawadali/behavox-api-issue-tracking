package com.behavox.workflow.util;

import java.util.UUID;

import com.behavox.workflow.exception.BadRequestException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UUIDUtil {

    public static UUID getUUIDFromString(String stringId) {
        UUID uuid;
        try {
            uuid = UUID.fromString(stringId);
        } catch (IllegalArgumentException e) {
            String message = "Unable to parse String to UUID : {}" + stringId;
            //We need to log the actual exception since we are throwing a different one to the client
            log.debug(message, e);
            throw new BadRequestException(message);
        }
        return uuid;
    }
}
