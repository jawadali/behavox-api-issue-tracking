package com.behavox.workflow.service;

import com.behavox.workflow.dto.UserDTO;
import org.springframework.stereotype.Service;

/**
 * Dummy Authentication Service
 */

@Service
public class AuthenticationService {

    /**
     * Just a dummy method to get the loggedIn user. In an ideal scenario, this will return
     * the actual logged in user
     */
    public UserDTO getLoggedInUser() {
        return UserDTO.builder()
            .id("loggedInUserId")
            .firstName("LOGGED")
            .lastName("IN")
            .avatarUrl(UserService.SYSTEM_DEFAULT_AVATAR_URL)
            .build();
    }
}
