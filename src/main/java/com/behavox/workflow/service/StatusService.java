package com.behavox.workflow.service;

import com.behavox.workflow.dao.StatusRepository;
import com.behavox.workflow.dto.StatusDTO;
import com.behavox.workflow.exception.ItemNotFoundException;
import com.behavox.workflow.model.Status;
import com.behavox.workflow.util.UUIDUtil;
import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.behavox.workflow.service.IssueService.NEW_ISSUE_STATUS_NAME;

@Service
public class StatusService {

    @Autowired
    private StatusRepository statusRepository;

    @Autowired
    private ConversionService conversionService;

    public StatusDTO createStatus(StatusDTO statusDTO) {
        Status status = conversionService.convert(statusDTO, Status.class);
        status.setId(UUID.randomUUID());
        status = statusRepository.save(status);
        return conversionService.convert(status, StatusDTO.class);
    }

    public List<StatusDTO> getAllStatuses() {
        List<Status> statusList = statusRepository.findAll();
        return statusList.stream().map(s -> conversionService.convert(s, StatusDTO.class)).collect(Collectors.toList());
    }

    public StatusDTO updateStatus(String statusId, StatusDTO statusDTO) {
        Status status = getStatusById(statusId);
        status.setName(statusDTO.getName());
        status = statusRepository.save(status);
        return conversionService.convert(status, StatusDTO.class);
    }

    public StatusDTO getStatus(String statusId) {
        Status status = getStatusById(statusId);
        return conversionService.convert(status, StatusDTO.class);
    }

    public Status getStatusById(String statusId) {
        UUID id = UUIDUtil.getUUIDFromString(statusId);
        Optional<Status> statusOptional = statusRepository.findById(id);
        if (!statusOptional.isPresent()) {
            throw new ItemNotFoundException("Status not found with id: " + id);
        }
        return statusOptional.get();
    }

    public void deleteStatus(String statusId) {
        Status status = getStatusById(statusId);
        statusRepository.delete(status);
    }

    public List<StatusDTO> createDefaultStatuses() {

        Status open = Status.builder().id(UUID.randomUUID()).name(NEW_ISSUE_STATUS_NAME).transitions(Sets.newHashSet()).build();
        Status inProgress = Status.builder().id(UUID.randomUUID()).name("IN PROGRESS").transitions(Sets.newHashSet()).build();
        Status resolved = Status.builder().id(UUID.randomUUID()).name("RESOLVED").transitions(Sets.newHashSet()).build();
        Status closed = Status.builder().id(UUID.randomUUID()).name("CLOSED").transitions(Sets.newHashSet()).build();
        Status reopened = Status.builder().id(UUID.randomUUID()).name("REOPENED").transitions(Sets.newHashSet()).build();
        List<Status> allDefaultStatuses = Arrays.asList(open, inProgress, resolved, closed, reopened);

        open.getTransitions().addAll(Arrays.asList(inProgress.getId(), resolved.getId(), closed.getId()));
        inProgress.getTransitions().addAll(Arrays.asList(open.getId(), resolved.getId(), closed.getId()));
        resolved.getTransitions().addAll(Arrays.asList(closed.getId(), reopened.getId()));
        closed.getTransitions().add(reopened.getId());
        reopened.getTransitions().addAll(Arrays.asList(inProgress.getId(), closed.getId(), resolved.getId()));

        allDefaultStatuses = statusRepository.saveAll(allDefaultStatuses);
        return allDefaultStatuses.stream().map(s -> conversionService.convert(s, StatusDTO.class)).collect(Collectors.toList());
    }
}
