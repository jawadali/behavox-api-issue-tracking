package com.behavox.workflow.service;

import com.behavox.workflow.dto.UserDTO;
import org.springframework.stereotype.Service;

/**
 * Dummy User Service
 */

@Service
public class UserService {

    public static final String SYSTEM_DEFAULT_USER_ID = "SYSTEM_DEFAULT_USER_ID";
    public static final String SYSTEM_DEFAULT_AVATAR_URL = "SYSTEM_DEFAULT_AVATAR_URL";


    public UserDTO getSystemDefaultUser() {
        return UserDTO.builder()
            .id(SYSTEM_DEFAULT_USER_ID)
            .firstName("Unassigned").lastName("")
            .avatarUrl(SYSTEM_DEFAULT_AVATAR_URL)
            .build();
    }

    //Ideally should get the actual user from User but due to lack of time, assume
    public UserDTO getUserById(String id) {
        return getSystemDefaultUser();
    }

}
