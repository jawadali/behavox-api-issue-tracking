package com.behavox.workflow.service;

import com.behavox.workflow.NotificationTemplate;
import com.behavox.workflow.dao.IssueRepository;
import com.behavox.workflow.dto.AssignmentDTO;
import com.behavox.workflow.dto.IssueDTO;
import com.behavox.workflow.dto.TransitionDTO;
import com.behavox.workflow.enricher.IssueEnricher;
import com.behavox.workflow.exception.BadRequestException;
import com.behavox.workflow.exception.ItemNotFoundException;
import com.behavox.workflow.model.Issue;
import com.behavox.workflow.model.Status;
import com.behavox.workflow.util.UUIDUtil;
import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Service
public class IssueService {

    @Autowired
    private IssueRepository issueRepository;

    @Autowired
    private ConversionService conversionService;

    @Autowired
    private StatusService statusService;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private IssueEnricher issueEnricher;

    @Autowired
    private NotificationService notificationService;

    public static final String NEW_ISSUE_STATUS_NAME = "OPEN";

    /**
     * Creates a new Issue. If the issue is not assigned to a user, it will automatically be assigned to system default user.
     * The reporter will always be the user sending the request i.e. logged in user. Ideally
     */

    public IssueDTO createIssue(IssueDTO issueDTO) {
        Issue issue = conversionService.convert(issueDTO, Issue.class);
        if (StringUtils.isEmpty(issue.getAssigneeId())) {
            issue.setAssigneeId(UserService.SYSTEM_DEFAULT_USER_ID);
        }
        if (StringUtils.isEmpty(issue.getReporterId())) {
            issue.setReporterId(authenticationService.getLoggedInUser().getId());
        }
        issue.setId(UUID.randomUUID());
        String openStatusId = statusService.getAllStatuses().stream().filter(s -> s.getName().equals(NEW_ISSUE_STATUS_NAME)).findFirst().get().getId();
        issue.setStatusId(UUIDUtil.getUUIDFromString(openStatusId));
        issue = issueRepository.save(issue);
        issueDTO = conversionService.convert(issue, IssueDTO.class);
        issueEnricher.enrich(issueDTO);
        //This should be ideally the id list of all watchers of this issue, using assignee just as an example.
        Set<String> notificationReceivers = Sets.newHashSet();
        notificationService.sendNotification(notificationReceivers, NotificationTemplate.ISSUE_CREATED);
        return issueDTO;
    }

    public IssueDTO getIssue(String issueId) {
        Issue issue = getIssueById(issueId);
        IssueDTO issueDTO = conversionService.convert(issue, IssueDTO.class);
        issueEnricher.enrich(issueDTO);
        return issueDTO;
    }

    private Issue getIssueById(String issueId) {
        UUID id = UUIDUtil.getUUIDFromString(issueId);
        Optional<Issue> issueOptional = issueRepository.findById(id);
        if (!issueOptional.isPresent()) {
            throw new ItemNotFoundException("No issue found with id " + issueId);
        }
        return issueOptional.get();
    }

    /**
     * Updates an issue by updating it's title and description. Ideally this could update
     * other fields like points, fix versions, etc etc EXCEPT status and assignee because
     * we have separate REST endpoints and custom logic for them.
     * @param issueId
     * @param issueDTO
     * @return
     */

    public IssueDTO updateIssue(String issueId, IssueDTO issueDTO) {
        Issue issue = getIssueById(issueId);
        issue.setTitle(issueDTO.getTitle());
        issue.setDescription(issueDTO.getDescription());
        issue = issueRepository.save(issue);
        issueDTO = conversionService.convert(issue, IssueDTO.class);
        issueEnricher.enrich(issueDTO);
        //should be watchers list here as well
        Set<String> notificationReceivers = Sets.newHashSet();
        notificationService.sendNotification(notificationReceivers, NotificationTemplate.ISSUE_UPDATED);
        return issueDTO;
    }

    public void deleteIssue(String issueId) {
        Issue issue = getIssueById(issueId);
        issueRepository.delete(issue);
    }

    public IssueDTO assignIssue(String issueId, AssignmentDTO assignmentDTO) {
        Issue issue = getIssueById(issueId);
        issue.setAssigneeId(assignmentDTO.getAssigneeId());
        issue = issueRepository.save(issue);
        IssueDTO issueDTO = conversionService.convert(issue, IssueDTO.class);
        issueEnricher.enrich(issueDTO);

        //should be actual watchers list here as well
        Set<String> notificationReceivers = Sets.newHashSet();
        notificationService.sendNotification(notificationReceivers, NotificationTemplate.ISSUE_ASSIGNED);
        return issueDTO;
    }

    public IssueDTO unassignIssue(String issueId) {
        return assignIssue(issueId, AssignmentDTO.builder().assigneeId(UserService.SYSTEM_DEFAULT_USER_ID).build());
    }

    public IssueDTO changeIssueStatus(String issueId, TransitionDTO transitionDTO) {
        Issue issue = getIssueById(issueId);
        Status currentStatus = statusService.getStatusById(issue.getStatusId().toString());
        //To Validate that the nextStatus is actually a valid status.
        Status nextStatus = statusService.getStatusById(transitionDTO.getNextStatusId());
        if (currentStatus.getTransitions().contains(nextStatus.getId())) {
            issue.setStatusId(nextStatus.getId());
            issue = issueRepository.save(issue);
            IssueDTO issueDTO = conversionService.convert(issue, IssueDTO.class);
            issueEnricher.enrich(issueDTO);
            //should be actual watchers list here as well
            Set<String> notificationReceivers = Sets.newHashSet();
            notificationService.sendNotification(notificationReceivers, NotificationTemplate.ISSUE_CHANGE_STATUS);
            return issueDTO;
        } else {
            throw new BadRequestException("Cannot transition from " + currentStatus.getName() + " to " + nextStatus.getName());
        }
    }
}
