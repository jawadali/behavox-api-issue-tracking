package com.behavox.workflow.service;

import com.behavox.workflow.NotificationTemplate;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * An empty Notification Service to describe some async operations while processing requests on issues.
 * The implementation depends on how the rest of the application is set up. In a microservices world,
 * this could be an HTTP client(for example FeignClient) that make a call to a notification microservice that
 * handles all push, in-app and email notifications.
 */

@Service
public class NotificationService {

    public void sendNotification(Set<String> receivers, NotificationTemplate template) {
        //Submit to queue(RabbitMQ/ActiveMQ etc) or do a REST call to another microservice depending on implementation.
    }
}
