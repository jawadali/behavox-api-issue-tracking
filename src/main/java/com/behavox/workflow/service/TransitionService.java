package com.behavox.workflow.service;

import com.behavox.workflow.dto.StatusDTO;
import com.behavox.workflow.dto.TransitionDTO;
import com.behavox.workflow.exception.BadRequestException;
import com.behavox.workflow.model.Status;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Service
@Slf4j
public class TransitionService {

    @Autowired
    private ConversionService conversionService;

    @Autowired
    private StatusService statusService;

    public void addTransition(String statusId, TransitionDTO transitionDTO) {
        Status status = statusService.getStatusById(statusId);
        Status nextStatus = statusService.getStatusById(transitionDTO.getNextStatusId());
        if (status.getTransitions() == null) {
            status.setTransitions(Sets.newHashSet());
        }
        status.getTransitions().add(nextStatus.getId());
        statusService.updateStatus(status.getId().toString(), conversionService.convert(status, StatusDTO.class));
    }

    public void removeTransition(String statusId, TransitionDTO transitionDTO) {
        Status status = statusService.getStatusById(statusId);
        Status statusToRemove = statusService.getStatusById(transitionDTO.getNextStatusId());
        if (CollectionUtils.isEmpty(status.getTransitions()) || !status.getTransitions().contains(statusToRemove.getId())) {
            throw new BadRequestException("Status " + statusId + " does not contain a transition with statusId " + statusToRemove.getId());
        }
        status.getTransitions().remove(statusToRemove.getId());
        statusService.updateStatus(status.getId().toString(), conversionService.convert(status, StatusDTO.class));
    }

}
