package com.behavox.workflow.util;

import static org.junit.Assert.assertNotNull;

import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.behavox.workflow.exception.BadRequestException;

@RunWith(SpringJUnit4ClassRunner.class)
public class UUIDUtilTest {

    @Test
    public void testValidUUIDStringReturns() {
        String validUUID = UUID.randomUUID().toString();
        UUID uuid = UUIDUtil.getUUIDFromString(validUUID);
        assertNotNull(uuid);
    }

    @Test(expected = BadRequestException.class)
    public void testInvalidUUIDStringThrowsException() {
        String invalidUUID = "invalid_uuid";
        UUID uuid = UUIDUtil.getUUIDFromString(invalidUUID);
    }
}
