package com.behavox.workflow.service;

import com.behavox.workflow.dao.IssueRepository;
import com.behavox.workflow.dto.*;
import com.behavox.workflow.enricher.IssueEnricher;
import com.behavox.workflow.exception.ItemNotFoundException;
import com.behavox.workflow.model.Issue;
import com.behavox.workflow.model.Status;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.core.convert.ConversionService;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(SpringJUnit4ClassRunner.class)
public class IssueServiceTest {

    @Mock
    private IssueRepository issueRepository;

    @Mock
    private ConversionService conversionService;

    @Mock
    private AuthenticationService authenticationService;

    @Mock
    private StatusService statusService;

    @Mock
    private IssueEnricher enricher;

    @Mock
    private NotificationService notificationService;

    @InjectMocks
    private IssueService classUnderTest;

    @Before
    public void beforeEachTest() {
        initMocks(this);
    }

    @Test
    public void testCreateIssue() {
        IssueDTO issueDTO = Mockito.mock(IssueDTO.class);
        Issue issue = Mockito.mock(Issue.class);
        when(authenticationService.getLoggedInUser()).thenReturn(UserDTO.builder().id("1").build());
        when(conversionService.convert(any(IssueDTO.class), eq(Issue.class))).thenReturn(issue);
        when(conversionService.convert(any(Issue.class), eq(IssueDTO.class))).thenReturn(issueDTO);
        StatusDTO defaultStatus = StatusDTO.builder().id(UUID.randomUUID().toString()).name("OPEN").build();
        when(statusService.getAllStatuses()).thenReturn(Lists.newArrayList(defaultStatus));

        classUnderTest.createIssue(issueDTO);
        verify(conversionService, times(1)).convert(any(), eq(Issue.class));
        verify(conversionService, times(1)).convert(any(), eq(IssueDTO.class));
        verify(issueRepository, times(1)).save(any(Issue.class));
        verify(notificationService, times(1)).sendNotification(anySet(), any());
    }

    @Test
    public void testGetIssue() {
        Issue issue = Mockito.mock(Issue.class);
        IssueDTO issueDTO = Mockito.mock(IssueDTO.class);
        UUID id = UUID.randomUUID();
        when(issueRepository.findById(id)).thenReturn(Optional.of(issue));
        when(conversionService.convert(issue, IssueDTO.class)).thenReturn(issueDTO);

        classUnderTest.getIssue(id.toString());

        verify(conversionService, times(1)).convert(any(), eq(IssueDTO.class));
    }

    @Test(expected = ItemNotFoundException.class)
    public void testGetIssueThrowsExceptionWhenNotFound() {
        when(issueRepository.findById(any())).thenReturn(Optional.empty());

        classUnderTest.getIssue(UUID.randomUUID().toString());
    }

    @Test
    public void testUpdateIssue() {
        Issue issue = Mockito.mock(Issue.class);
        UUID id  = UUID.randomUUID();
        IssueDTO issueDTO = IssueDTO.builder().id(id.toString()).title("title").description("desc").build();
        when(conversionService.convert(issue, IssueDTO.class)).thenReturn(issueDTO);

        when(issueRepository.findById(id)).thenReturn(Optional.of(issue));
        classUnderTest.updateIssue(id.toString(), issueDTO);
        verify(issueRepository, times(1)).save(issue);
        verify(conversionService, times(1)).convert(any(), eq(IssueDTO.class));
        verify(notificationService, times(1)).sendNotification(anySet(), any());
    }

    @Test
    public void testDeleteIssue() {
        Issue issue = Mockito.mock(Issue.class);
        UUID id = UUID.randomUUID();
        when(issueRepository.findById(id)).thenReturn(Optional.of(issue));

        classUnderTest.deleteIssue(id.toString());

        verify(issueRepository, times(1)).delete(issue);
    }

    @Test
    public void testUpdateStatus() {
        Issue issue = Issue.builder().id(UUID.randomUUID()).statusId(UUID.randomUUID()).build();
        TransitionDTO transitionDTO = TransitionDTO.builder().nextStatusId(UUID.randomUUID().toString()).build();
        Status currentStatus = Mockito.mock(Status.class);
        Status nextStatus = Mockito.mock(Status.class);
        when(statusService.getStatusById(issue.getStatusId().toString())).thenReturn(currentStatus);
        when(statusService.getStatusById(transitionDTO.getNextStatusId())).thenReturn(nextStatus);
        Set<UUID> transitions = Sets.newHashSet(nextStatus.getId());
        when(currentStatus.getTransitions()).thenReturn(transitions);

        when(issueRepository.findById(issue.getId())).thenReturn(Optional.of(issue));

        classUnderTest.changeIssueStatus(issue.getId().toString(), transitionDTO);

        verify(statusService, times(2)).getStatusById(anyString());
        verify(issueRepository, times(1)).save(any(Issue.class));
        verify(conversionService, times(1)).convert(any(), eq(IssueDTO.class));
        verify(notificationService, times(1)).sendNotification(anySet(), any());
    }

    @Test
    public void testAssignIssue() {
        Issue issue = Mockito.mock(Issue.class);
        IssueDTO issueDTO = Mockito.mock(IssueDTO.class);
        UUID id  = UUID.randomUUID();
        AssignmentDTO assignmentDTO = AssignmentDTO.builder().assigneeId(id.toString()).build();
        when(conversionService.convert(issue, IssueDTO.class)).thenReturn(issueDTO);
        when(issueRepository.findById(id)).thenReturn(Optional.of(issue));
        classUnderTest.assignIssue(id.toString(), assignmentDTO);
        verify(issueRepository, times(1)).save(issue);
        verify(conversionService, times(1)).convert(any(), eq(IssueDTO.class));
        verify(notificationService, times(1)).sendNotification(anySet(), any());
    }
}
