package com.behavox.workflow.service;

import com.behavox.workflow.dao.StatusRepository;
import com.behavox.workflow.dto.StatusDTO;
import com.behavox.workflow.exception.ItemNotFoundException;
import com.behavox.workflow.model.Status;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.core.convert.ConversionService;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(SpringJUnit4ClassRunner.class)
public class StatusServiceTest {

    @Mock
    private StatusRepository statusRepository;

    @Mock
    private ConversionService conversionService;

    @InjectMocks
    private StatusService classUnderTest;

    @Before
    public void beforeEachTest() {
        initMocks(this);
    }

    @Test
    public void testCreateStatus() {
        StatusDTO statusDTO = Mockito.mock(StatusDTO.class);
        Status status = Mockito.mock(Status.class);
        when(conversionService.convert(statusDTO, Status.class)).thenReturn(status);

        classUnderTest.createStatus(statusDTO);

        verify(statusRepository, times(1)).save(status);
        verify(conversionService, times(1)).convert(any(), eq(StatusDTO.class));
    }

    @Test
    public void testUpdateStatus() {
        UUID statusId = UUID.randomUUID();
        StatusDTO statusDTO = Mockito.mock(StatusDTO.class);
        when(statusDTO.getName()).thenReturn("NEW_NAME");
        Status status = Mockito.mock(Status.class);
        when(statusRepository.findById(statusId)).thenReturn(Optional.of(status));
        when(conversionService.convert(statusDTO, Status.class)).thenReturn(status);

        classUnderTest.updateStatus(statusId.toString(), statusDTO);

        verify(statusRepository, times(1)).findById(statusId);
        verify(statusRepository, times(1)).save(status);
        verify(conversionService, times(1)).convert(any(), eq(StatusDTO.class));

    }

    @Test
    public void testGetStatus() {
        UUID statusId = UUID.randomUUID();
        Status status = Mockito.mock(Status.class);
        StatusDTO statusDTO = Mockito.mock(StatusDTO.class);
        when(statusRepository.findById(statusId)).thenReturn(Optional.of(status));
        when(conversionService.convert(status, StatusDTO.class)).thenReturn(statusDTO);

        classUnderTest.getStatus(statusId.toString());

        verify(statusRepository, times(1)).findById(statusId);
        verify(conversionService, times(1)).convert(any(), eq(StatusDTO.class));
    }

    @Test(expected = ItemNotFoundException.class)
    public void testGetStatusThrowsExceptionWhenNotFound() {
        when(statusRepository.findById(any())).thenReturn(Optional.empty());

        classUnderTest.getStatusById(UUID.randomUUID().toString());
    }

    @Test
    public void testDeleteStatus() {
        UUID statusId = UUID.randomUUID();
        Status status = Mockito.mock(Status.class);
        when(statusRepository.findById(statusId)).thenReturn(Optional.of(status));

        classUnderTest.deleteStatus(statusId.toString());

        verify(statusRepository, times(1)).findById(statusId);
        verify(statusRepository, times(1)).delete(any());
    }
}
